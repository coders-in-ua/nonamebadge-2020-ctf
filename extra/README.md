# EXTRA

## Firmware to ELF

To make easier firmware loading to debuggers I created special tool to convert firmware to ELF.

It automatically loads firmware segments to proper offset plus more - loads some parts of badge ROM and adds some symbols to ELF.

## Signatures

After loading ELF to debugger the good point is to get more symbolic information.

Using some information from ESP-IDF SDK and using signatures.


## JTAG

Badge has place for JTAG connector which not soldered.

Why to not solder it and connect JTAG adapter?




./esp32knife.py --chip=esp32 load_from_device --port=auto --baud=2000000 -e
./esp32knife.py -m=nncbadge2020 --chip=esp32 load_from_device --port=auto --baud=2000000 -e
