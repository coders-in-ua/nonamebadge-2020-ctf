#!/usr/bin/env python
import os, sys

import string
import itertools


alphabet = "0123456789abcdef"
a = sorted(map(ord, alphabet))

s1 = sum(map(ord,"flag{z3}"))

fs = "flag{z3*********}"

#s for sum(flag[i]) i=0..16 except 7,8
smin = s1+7*min(a)
smax = s1+7*min(a)

a78=[]
for i in range(len(a)):
    for j in range(i, len(a)):
        if ( a[i]*a[j]):
            a78.append( (a[i], a[j]) )

print("Number of possible candidates for a7, a8: ", len(a78))

f  = list(map(ord, fs))

cnt = 0
for a7,a8 in a78:
    f[7]=a7
    f[8]=a8
    for p in itertools.combinations( a, 7 ):
        if s1+a7+a8+sum(p)==(a7*a8)//4:
            f[9:16] = p
            print("Found : {}".format("".join(map(chr, f))))
            cnt+=1
            if cnt==10:
                print("--- first 10 flags shown ---")
                exit(0)


