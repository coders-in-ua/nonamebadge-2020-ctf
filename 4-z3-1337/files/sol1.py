#!/usr/bin/env python
import os, sys

import string
import itertools


f1="flag{z3}"
f2="flag{z3000000000}"

s1 = sum( ord(s) for s in f1) 
print(s1)
cnt=0
for p in itertools.permutations( "0123456789abcdef", 9 ):
    s2=sum(map(ord,p))
    s=s1+s2
    t=ord(p[0])*ord(p[1])//4
    #print(s2,p,t)
    if s==t:
        print(s2,p,t)
    cnt+=1
    if cnt%1000000==0:
        print("Checked variants: {}".format(cnt))


