#!/usr/bin/env python
from z3 import *

solver = Solver()
bytes = []
for i in range(17):
    bytes.append(BitVec("b%i" % i, 16))

bytes[0] = 102   # f
bytes[1] = 108   # l
bytes[2] = 97    # a
bytes[3] = 103   # g
bytes[4] = 123   # {
bytes[5] = 122   # z
bytes[6] = 51    # 3
#bytes[7] = 0     # ?
#bytes[8] = 0     # ?
#bytes[9] = 0     # ?
#bytes[10] = 0    # ?
#bytes[11] = 0    # ?
#bytes[12] = 0    # ?
#bytes[13] = 0    # ?
#bytes[14] = 0    # ?
#bytes[15] = 0    # ?
bytes[16] = 125  # }


for i in range(7,16):
    solver.add(bytes[i] >= ord('0'), bytes[i] <= ord('z'))

solver.add(bytes[7]*bytes[8]/4 == sum(bytes))

print(solver.check())
m = solver.model()

result = 'flag{z3'
for i in range(7, 16):
    result += chr(m[bytes[i]].as_long())
result += '}'

print(result)

# s = sum( map(ord, result))
# m = ( ord(result[7])*ord(result[8]) ) >> 2
# print(s,m)