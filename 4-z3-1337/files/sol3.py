#!/usr/bin/env python3
import os, sys
from z3 import *

fmask="flag{z3*********}"

a = [None] * len(fmask)

s = Solver()
for i,c in enumerate(fmask):
    if c=='*':
        a[i] = Int('a%i' % i)
        # for ASCII
        s.add( ord('0') <= a[i], a[i] <= ord('z'))
        # for HEX
        # s.add( Or( 
        #                 And( ord('0') <= a[i], a[i] <= ord('9')), 
        #                 And( ord('a') <= a[i], a[i] <= ord('f')) 
        #         ) 
        #       )
    else:
        a[i] = ord(c)
s.add( sum(a) == ( (a[7]*a[8])/4) )

cnt =0 
while s.check():
    m = s.model()
    res=[None]*len(fmask)
    for i,b in enumerate(a):
        if type(b) is int:
            v = b
        else:
            v = m[b].as_long()
        res[i]=v
    print("Flag : {}".format("".join(map(chr,res))))
    cnt+=1
    if cnt==10:
        print("--- first 10 flags shown ---")
        exit(0)

    # ss=sum(res)
    # tt=(res[7]*res[8])//4
    # print("-> check if correct : {}".format(ss==tt))



