# 4. Z3 1337

## Step 1. Get patch code

Getting flag in [Fiasco TransPort](2-fiasco-transport/README.md) task we also founf patch.zip file.
The same file could be extracted using binwalk tool.
Inside this zip file we found some patch.txt file:
```asm
┌ 135: sym._z3_31337 ((char *) flag);
│           0x400d614c      364100         entry a1, 32
│           0x400d614f      20a220         or a10, a2, a2              
│           0x400d6152      8141e8         l32r a8, 0x400d0258         ; a8=0x400014c0 sym.strlen
│           0x400d6155      e00800         callx8 a8                   ; a10(0x0, 0x0, 0x0, 0x0)
│           0x400d6158      1c18           movi.n a8, 17               ; a8=0x11 sar
│           0x400d615a      3d02           mov.n a3, a2                ; a3=0x0
│           0x400d615c      0c02           movi.n a2, 0                ; a2=0x0
│       ┌─< 0x400d615e      879a6f         bne a10, a8, 0x400d61d1     ; pc=0x400d61d1 -> 0x3600f01d
│       │   0x400d6161      920300         l8ui a9, a3, 0              ; a9=0xff
│       │   0x400d6164      82a066         movi a8, 102                ; a8=0x66
│      ┌──< 0x400d6167      879966         bne a9, a8, 0x400d61d1      ; pc=0x400d61d1 -> 0x3600f01d 
│      ││   0x400d616a      920301         l8ui a9, a3, 1              ; a9=0xff
│      ││   0x400d616d      82a06c         movi a8, 108                ; a8=0x6c
│     ┌───< 0x400d6170      87995d         bne a9, a8, 0x400d61d1      ; pc=0x400d61d1 -> 0x3600f01d 
│     │││   0x400d6173      920302         l8ui a9, a3, 2              ; a9=0xff
│     │││   0x400d6176      82a061         movi a8, 97                 ; a8=0x61
│    ┌────< 0x400d6179      879954         bne a9, a8, 0x400d61d1      ; pc=0x400d61d1 -> 0x3600f01d 
│    ││││   0x400d617c      920303         l8ui a9, a3, 3              ; a9=0xff
│    ││││   0x400d617f      82a067         movi a8, 103                ; a8=0x67
│   ┌─────< 0x400d6182      87994b         bne a9, a8, 0x400d61d1      ; pc=0x400d61d1 -> 0x3600f01d 
│   │││││   0x400d6185      920304         l8ui a9, a3, 4              ; a9=0xff
│   │││││   0x400d6188      82a07b         movi a8, 123                ; a8=0x7b
│  ┌──────< 0x400d618b      879942         bne a9, a8, 0x400d61d1      ; pc=0x400d61d1 -> 0x3600f01d 
│  ││││││   0x400d618e      920305         l8ui a9, a3, 5              ; a9=0xff
│  ││││││   0x400d6191      82a07a         movi a8, 122                ; a8=0x7a
│ ┌───────< 0x400d6194      879939         bne a9, a8, 0x400d61d1      ; pc=0x400d61d1 -> 0x3600f01d 
│ │││││││   0x400d6197      920306         l8ui a9, a3, 6              ; a9=0xff
│ │││││││   0x400d619a      3c38           movi.n a8, 51               ; a8=0x33
│ ────────< 0x400d619c      879931         bne a9, a8, 0x400d61d1      ; pc=0x400d61d1 -> 0x3600f01d 
│ │││││││   0x400d619f      920310         l8ui a9, a3, 16             ; a9=0xff
│ │││││││   0x400d61a2      82a07d         movi a8, 125                ; a8=0x7d
│ ────────< 0x400d61a5      879928         bne a9, a8, 0x400d61d1      ; pc=0x400d61d1 -> 0x3600f01d 
│ │││││││   0x400d61a8      820307         l8ui a8, a3, 7              ; a8=0xff
│ │││││││   0x400d61ab      b20308         l8ui a11, a3, 8             ; a11=0xff
│ │││││││   0x400d61ae      ad02           mov.n a10, a2               ; a10=0x0
│ │││││││   0x400d61b0      1c09           movi.n a9, 16               ; a9=0x10 loc..locsz
│ ────────< 0x400d61b2      768908         loop a9, 0x400d61be         
│ │││││││   0x400d61b5      aac3           add.n a12, a3, a10          ; a12=0x0
│ │││││││   0x400d61b7      c20c00         l8ui a12, a12, 0            ; a12=0xff
│ │││││││   0x400d61ba      1baa           addi.n a10, a10, 1          ; a10=0x1 a4
│ │││││││   0x400d61bc      ca22           add.n a2, a2, a12           ; a2=0xff
│ │││││││   ; CODE XREF from sym._z3_31337 @ 0x400d61b2
│ ────────> 0x400d61be      b088c1         mul16u a8, a8, a11
│ │││││││   0x400d61c1      0c13           movi.n a3, 1                ; a3=0x1 a4
│ │││││││   0x400d61c3      808221         srai a8, a8, 2              ; a8 >> 2
│ │││││││   0x400d61c6      2028c0         sub a2, a8, a2              ; a2=0x0
│ │││││││   0x400d61c9      0c08           movi.n a8, 0                ; a8=0x0
│ │││││││   0x400d61cb      208383         moveqz a8, a3, a2           ; a8=0x1 a4
│ │││││││   0x400d61ce      802074         extui a2, a8, 0, 8          ; a2=0x1 a4
│ │││││││   ; XREFS: CODE 0x400d615e  CODE 0x400d6167  CODE 0x400d6170  CODE 0x400d6179  CODE 0x400d6182  CODE 0x400d618b  
│ │││││││   ; XREFS: CODE 0x400d6194  CODE 0x400d619c  CODE 0x400d61a5  
└ └└└└└└└─> 0x400d61d1      1df0           retw.n
```

Looks like source code of flag checking function leaked.

Let's try use this knowledge.

## Step 2. Recover algorithm

Problem that there are about no decompiler for XTensa code yet.

Let's get XTensa instruction set and try recover algorithm line by line.

I created commented code:

```asm
sym._z3_31337 ((char *) flag); flag - poinnter to string buffer
entry a1, 32
or a10, a2, a2              ; a10=a2=flag 
l32r a8, 0x400d0258         ; a8=&strlen
callx8 a8                   ; a10=strlen(flag) ( callx8 : returns single result in a10)
movi.n a8, 17               ; a8=17
mov.n a3, a2                ; a3=flag
movi.n a2, 0                ; a2=0x0 (res=0)
bne a10, a8, 0x400d61d1     ; if a10!=a8 (flag_len!=17) goto END
l8ui a9, a3, 0              ; a9=a3[0]
movi a8, 102                ; a8=102
bne a9, a8, 0x400d61d1      ; if a9!=a8 goto END
...
l8ui a9, a3, 16             ; a9=16
movi a8, 125                ; a8=0x7d
bne a9, a8, 0x400d61d1      ; if a9!=a8 goto END
l8ui a8, a3, 7              ; a8=flag[7]
l8ui a11, a3, 8             ; a11=flag[8]
mov.n a10, a2               ; a10=a2 => a10=0 (check above)
movi.n a9, 16               ; a9=16

loop a9, 0x400d61be         : Loop 16+1 times up to 0x400d61be address, let's name it L_LOOP
add.n a12, a3, a10          ; a12=a3+a10=&flag + a10
l8ui a12, a12, 0            ; a12=flag[a10]
addi.n a10, a10, 1          ; a10+=1
add.n a2, a2, a12           ; a2+=a12 i.e a2+=flag[a10]
L_LOOP:

mul16u a8, a8, a11      ; a8*=a11 i.e. a8=flag[7]*flag[8]
movi.n a3, 1                ; a3=0x1
srai a8, a8, 2              ; a8 =>> 2 i.e. a8=(flag[7]*flag[8])>>2
sub a2, a8, a2              ; a2-=0a8
movi.n a8, 0                ; a8=0x0
moveqz a8, a3, a2           ; a8=a3 if a2==0
extui a2, a8, 0, 8          ; extending LSB bit to 8 bits, i.e. a2=255 if LSB of a8==1. 

END:

retw.n
```

Carefully checking this code we found:

1. Flag is looks like "flag{z3*********}" where "*" - unknown symbols, flag[]="flag{z3*********}"
2. Check is done: sum(flag[]) == (flag[7]*flag[8])>>2
3. We just have to find flag[i], i=7..15 such that condition in p.2 is true


## Step 3. Rock'n'Roll

### Variant 1. Naive

Try brute-force: [script 1](files/sol1.py) . But it is toooo slow, I wasn't able to get result with is.

### Variant 2. Some math

If you good with math you can get lttle-bit optimized solution: [script 2](files/sol2.py)

It gives correct flag but ... I intentionally put it here without any comments - try find how it works %)

### Variant 3. Let's z3 do math for use

Z3 - it is math solver which helps to solve tasks without need to deep in math.

Here are two script:

- naive like (with "usual" int arithmetics) : [script 3](files/sol3.py)
- optimized (with bit arithmetics) : [script 4](files/sol4.py)

Using Z3 you just need write p.1..3 (above) in acceptable by Z3 form and ask Z3 to find solution.

I.e. by fact all task descibed in z3 here:

```python
fmask="flag{z3*********}"
a = [None] * len(fmask)
s = Solver()
for i,c in enumerate(fmask):
    if c=='*':
        a[i] = Int('a%i' % i) # or a[i] = BitVec("a%i" % i, 16)
        # ASCII - which symbols allowed instead '*'
        s.add( ord('0') <= a[i], a[i] <= ord('z'))
    else:
        a[i] = ord(c)
s.add( sum(a) == ( (a[7]*a[8])/4) ) #main check
```
In the case of BitVec we should keep in mind, that single char is 8 bit value, multiplication of two 8-bit values can give 16-bit value.
That why minimal size of BitVec is 16.

### z3 and reverse

- [CrackMe with z3py](https://xakep.ru/2016/04/26/crackme-with-z3py/)
- [SAT/SMT by Example Dennis Yurichev](https://yurichev.com/writings/SAT_SMT_by_example.pdf)
- [Intro to Binary Analysis with Z3 and angr](https://github.com/FSecureLABS/z3_and_angr_binary_analysis_workshop)
- [Code & screenshots for various Z3 based CTF challenge writeups](https://github.com/sam-b/z3-stuff)
- [Writeup: Sharky CTF 2020 - Z3 Robot](https://cesena.github.io/2020/05/13/z3-robot/)
- [PicoCTF 2018 - Reverse Engineering writeups](https://shizz3r.blogspot.com/2019/03/picoctf-2018-reverse-engineering.html)
- [PicoCTF 2018 Writeup: Reversing](https://tcode2k16.github.io/blog/posts/picoctf-2018-writeup/reversing/)
