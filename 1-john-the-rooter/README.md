# 1. John the Rooter

You receive badge without any CTF firmware.
To participate in CTF you have to download CTF firmware from official CTF site, flash it and run terminal:


![CTF starts!](img/ctf_starts_00.png)

In terminal (imemdeatly after badge reboot) you will see login form:

![Login:](img/ctf_login.png)

Trying any login/password (sure, incorrect ones!) you will get into the badge console but without root rights:

![Help](img/ctf_help.png)


![Covidsweeper](img/ctf_covidsweeper_00.png)

You even can play into the mineswee...ups, covidsweeper game %) :

![Covidsweeper](img/ctf_covidsweeper_02.png)


We **need** get root access to continue CTF!!!

## Method 1. Brute force

In "some" official Telegram chat we could find next leaked screenshot:

![Login:](img/ctf_login_leaked.jpg)

I.e. login is known - "root".

Also we may suppose the password length - 10 chars.

Other hint is typed in the terminal - "Secrets lie in the shadow".

Checking official downloaded bin file for "shadow" strings gives nothing interesting.

But downloading full firmware from badge:

```bash
esptool.py -p /dev/ttyUSB0 -b 2000000 read_flash 0x0 0x400000 firmware_full.bin
```

and checking it for "shadow" ( for people who hasn't badge I put full firmware to the _firmware/2-CTF/full/firmaware_ctf_full_initial.bin_  ):

we will see 2 "shadow" occurences:

![Strings](img/shadow_full.png)

Checking file more carefully near the new "/etc/shadow" we will find root hash:

![Strings](img/shadow_hash1.png)

![Strings](img/shadow_hash2.png)

Checking found hash at https://crackstation.net/ , https://hashes.com/en/decrypt/hash etc gave no results.

Ok, let's bruteforce by dictionary (hm, no success) and by mask (I copied found hash to the nnc_hash file, also limited alphabet to lower/upper alpha+digits+"."+"/" - I found such string in the file and had thought that it is allowed for password chars):

```bash
hashcat -m 7400 -a 3 -1 ./?l?u?d nnc_hash ?1?1?1?1?1?1?1?1?1?1
```
gives estimated time equal to... "Next Big Bang (> 10 years)"...

Yep. The need something else....

## Method 2. Brute force with mask

My next (but not the best) idea was to find password requirements by reversing binary. I spent 4 hours and was already inside password checking function (as I thought) then the next hint appeared in the official chat:

![Hint!](img/password_hint.png)

At this moment I was here (checking "nnc_%02x%02x%02x" %) :

![Hint!](img/password_check_code.png)

Knowning password mask reduces the number of unknown symbols just to 6 hex digits:

```bash
hashcat -m 7400 -a 3 nnc_hash nnc_?h?h?h?h?h?h
```

Maximum estimated time is just ... 16 mins.

Relax, drink coffee and wait...

My password was found in ... 2 minutes and:

![Hint!](img/root_password.png)

BTW the same hint could be found checking possible passwords masks of length 10 in the strings:

![Hint!](img/password_mask.png)

This time it might have worked.

PS: each badge has it's own root password generated at first CTF firmware startup.

## Method 3. Replace hash in NVS

Not big secret that 99.9% of CTF participants got root access using previous method.

But exists other way...

If check hash position in full firmware:

![Hash pos](img/hash_pos.png)

i.e. 0xa3ff

and lookup in partition table (the can get it via version command on badge):

![Partitions](img/cmd_version.png)

we will see that hash stored in the NVS partition.

You can't just modify hash in NVS.

Sure you *CAN* but due to NVS protected with few CRC without changing CRC data will be considered as corrupted and ignored (in our case badge will regenerate new root password).

For example two different passwords cause additional 8 bytes changed:

![Partitions](img/nvs_crc32.png)

Due to SDK source are mostly opensource you can restore exact CRC calculation method (hint: the first CRC32 is total entry CRC, the second one is just string CRC)

But exact CRC calculation can be changed in the future vesrions of SDK.

Let's force SDK do job for us %)

### Via custom software upload

The first method is write custom firmware which modifies NVS (sure you must use the same SDK as athours of Badge used i.e. 4.1).

I.e.:
1. Make full badge firmware backup (due to the next operations are risky).
2. Download APP (ota_0) partition.
3. Create empty ESP-IDF project (or use any example in the SDK directory). Don't forget make partitions tables start from 0x9000 (we will need it later %)
4. Rewrite main function.
5. Compile.
6. Upload only APP partition to badge instead ota_0.
7. Reset badge
8. Upload ota_o from done in p.2 backup
9. If badge bricked - restore from full backup %) :

![Partitions](img/nvs_anvol.png)

### Via custom NVS upload

Espressif SDK have very good tool named **NVS Partition Generator Utility**

https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-reference/storage/nvs_partition_gen.html

It allows generate NVS partition from text .cvs file .

Only one question is how to contruct .cvs file compatible with our firmware. To do this I wrote special script which reconstructs .cvs file from NVS binary. Script is still beta and it seems to be not fully restore .cvs from bin but... allows to rewrite hash %)

All related scripts are in the [src](1-john-the-rooter/src) and [firmware](firmware/) folders

Now:

1. Create full backup:

```bash 
./read_full.sh firmware_full_no_root.bin
```

2. Read NVS partition:

```bash 
./nvs_read.sh nvs_orig.bin
```

3. Convert it to cvs

```bash
./nvs2cvs.py -t cvs nvs_orig.bin > nvs_orig.cvs
```

4. Copy and patch with own hash:
```bash
cp nvs_orig.cvs nvs_patched.cvs
```
I replaced password hash in nvs_patched.cvs with hash from AnVol screenshot above %) i.e. I'm going to change password to 123

5. Generate bin from cvs:

```bash
./nvs_partition_gen.py generate nvs_patched.cvs nvs_patched.bin 0x4000
```

6. flash

```bash
./nvs_write.sh nvs_patched.bin
```

7. Connect to Badge via terminal, you will see "BSP badge type detection" warning (it seems to be not all data from original NVS were transfered correctly %), push RIGHT CHIP on the badge, enter "root" and "123" and...

<div align="center">
![Root](img/nvs_success.png))

![Root](img/im-root.png)
</div>


PS: more NVS related info:

https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-reference/storage/nvs_flash.html#structure-of-entry

https://github.com/tenable/esp32_image_parser

https://www.lucadentella.it/en/2017/06/03/esp32-19-nvs/

https://esp32.com/viewtopic.php?t=7951




