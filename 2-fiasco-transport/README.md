# 2. Fiasco TransPort

Very simple, just in pictures ^) :

## Scan for ports

![nmap](img/nmap.png)

## Get files

Via ftp:

![ftp](img/ftp.png)

or wget:

![nmap](img/wget.png)

## Check flag
 
![flag](img/flag.png)

(really flag was sent to server as only you had downloaded flag.txt)

