# NoNameBadge 2020 CTF

NoNameCon 2020 CTF write-ups

(under work!)

[NoNameBadge official info](https://blog.techmaker.ua/posts/nonamecon-badge-2020/)

[Official Leaderboard](https://ctf.techmaker.ua/scores)

( _af79a2e1b6e5cfa0ea71ee74_ is me ^) )

# Contents

- [1. John the Rooter]( 1-john-the-rooter/README.md )
- [2. Fiasco TransPort]( 2-fiasco-transport/README.md )
- [3. Absolutely ClueLess]( 3-absolutely-clueless/README.md )
- [4. Z3 1337]( 4-z3-1337/README.md )
- [5. Meet Me]( 5-meet-me/README.md )
- [6. Take your Time]( 6-take-your-time/README.md )
- [7. Obfuscated Mr BeanWalker]( 7-obfuscated-mr-beanwalker/README.md )
- [8. Unsolvable]( 8-unsolvable/README.md )
- [EXTRA](extra/README.md)

