#!/usr/bin/env python3
import sys
import argparse
from binascii import *
import copy
from collections import defaultdict

PATTERN = "Congrats!"
def try_decode(buf, pos, key, minlen=3):
    
    ic = 0
    ip = pos
    res = ""
    while ip<len(buf) and buf[ip]:
        d = buf[ip] ^ key[ic % len(key) ]
        c = chr(d)
        if d>=9 and d<=127:
        #if c.isprintable():
            res += c
            ic+=1
            ip+=1
            continue
        return (False, ip+1, None)

    if len(res)>=minlen:
        return (True, ip+1, res)
    return (False, ip+1, None)
    
def decrypt_pattern(s):
    return bytes( c^ord(PATTERN[i]) for i,c in enumerate(s))

def decrypt(s,k):
    minlen = min(len(s), len(k))
    return bytes( s[i]^k[i] for i in range(minlen))

def decode(input_file, output_file):

    buf = None
    with open(input_file,"rb") as fin:
        buf = fin.read()

    print("=============================================================")
    print("====================  AUTO   ================================")
    print("=============================================================")

    GROUPS = defaultdict(list)
    STRINGS = []
    LP = len(PATTERN)

    # get all possible strings and group them
    i = 0
    while i < len(buf):
        p = buf.find(b'\0', i)
        if p==-1:
            break
        if p-i>LP:
            s = buf[i:p]
            GROUPS[s[:LP]].append(s)
            STRINGS.append(s)
            #print( buf[i:p])
        i = max(i+1, p+1)

    #leave only groups with>=6 strings
    GROUPS = dict( filter( lambda g: len(g[1])>=6, GROUPS.items() ) )


    keys = []
    for k,g in GROUPS.items():
        key = decrypt_pattern(k)
        #try locate key in binary
        p=buf.find(key)
        if p==-1:
            continue
        print("POSSIBLE KEY FOUND:")
        print("{:08x} : {}".format(p,key))
        for s in g:
            print(decrypt(s, buf[p:p+64]))
        keys.append(buf[p:p+64])

    #try decoed all strings using found keys
    with open(output_file,"wt+") as fout:
        for k in keys:
            for s in STRINGS:
                r = decrypt(s, k)
                try:
                    fout.write(r.decode('utf-8'))
                    fout.write("\n")
                except:
                    pass


    return

if __name__ == '__main__':
    try:
        parser = argparse.ArgumentParser(add_help=True, description='ELF bin disasemble')
        parser.add_argument("input_file",  help="input encoded file")
        parser.add_argument("output_file", help="output decoded file")
        if len(sys.argv) == 1:
            print("Wrong arguments!!!")
            parser.print_help()
            exit(0)
        args = parser.parse_args()
        decode(args.input_file, args.output_file)
    except Exception as inst:
        print(type(inst))
        print(inst.args)
        print(inst)

