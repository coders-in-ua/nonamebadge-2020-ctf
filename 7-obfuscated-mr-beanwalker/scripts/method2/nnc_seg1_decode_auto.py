#!/usr/bin/env python3
import sys
import argparse
from binascii import *
import copy


def try_decode(buf, pos, key, minlen=3):
    
    ic = 0
    ip = pos
    res = ""
    while ip<len(buf) and buf[ip]:
        d = buf[ip] ^ key[ic % len(key) ]
        c = chr(d)
        if d>=9 and d<=127:
        #if c.isprintable():
            res += c
            ic+=1
            ip+=1
            continue
        return (False, ip+1, None)

    if len(res)>=minlen:
        return (True, ip+1, res)
    return (False, ip+1, None)
    
def decode(input_file, output_file):

    buf1 = None
    with open(input_file,"rb") as fin:
        buf1 = fin.read()
    buf2 = copy.copy(buf1)

    addr_seg1 = 0x3f400020
    addr_enc_key = 0x3f400db4 - addr_seg1
    
    key1 = [ buf1[addr_enc_key+i] for i in range(64)]

    print("=============================================================")
    print("====================  AUTO   ================================")
    print("=============================================================")

    i = 0
    while i < len(buf1):
        f, j, s = try_decode( buf1, i, key1)
        if f:
            print("{:08x} {:02x} = {}".format( addr_seg1+i, j-i, s))
        i=j

    return

if __name__ == '__main__':
    try:
        parser = argparse.ArgumentParser(add_help=True, description='ELF bin disasemble')
        parser.add_argument("input_file",  help="input encoded file")
        parser.add_argument("output_file", help="output decoded file")
        if len(sys.argv) == 1:
            print("Wrong arguments!!!")
            parser.print_help()
            exit(0)
        args = parser.parse_args()
        decode(args.input_file, args.output_file)
    except Exception as inst:
        print(type(inst))
        print(inst.args)
        print(inst)

