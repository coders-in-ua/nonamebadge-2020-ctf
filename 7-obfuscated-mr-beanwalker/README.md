# 7. Obfuscated Mr BeanWalker

When I started doing this task I had already parsed/disasembled firmware.

During static analyzing of app_main function and checking new (comparing to initial, non_CTF, badge firmware) I;ve found new (CTF related) functions (marked as _subnew_XX_ ):

![AppMain](img/appmain_00.png)

Diving into _subnew_1_ ... 

![AppMain](img/subnew_1.png)

I saw a sequence of calls to some function (named later _sb_decode_ ), diving into the _sb_decode_ ...

![AppMain](img/sbdecode.png)

Now I had two variants how to check what exactly this function is doing

## Method 1. JTAG 

When I started doing this task I had already connected JTAG to it (will be described in the Extra section of this write-ups).

All that I have to do - just break into this function and check inputs/outputs:

![JTAG](img/jtag_00.png)

![JTAG](img/jtag_01.png)

![JTAG](img/jtag_02.png)

![JTAG](img/jtag_03.png)

![JTAG](img/jtag_04.png)

![JTAG](img/jtag_05.png)

(c) **from_TechMaker_with_Love**

## Method 2. Get key with static code analysis and decode

If you haven't yet connected JTAG can do via decompiling sb_decode (like you did in the **z3-1337** task), check comments:

![AppMain](img/sbdecode.png)

ofs_enc_key - address of decryption XOR key.

Just write [decoding script](7-obfuscated-mr-beanwalker/scripts/method2/nnc_seg1_decode_auto.py) and run it:

```bash
./nnc_seg1_decode_auto.py firmware_partitions.bin.3.ota_0.seg1 firmware_partitions.bin.3.ota_0.decoded


=============================================================
====================  AUTO   ================================
=============================================================
3f400020 05 = 7X#+
3f40081e 05 = fm@
3f400861 05 = 5<`
3f400c80 68 = {"flags":["%s","%s","%s","%s","%s","%s","%s","%s"],"timeattack":{"solved":false,"rand":%d,"sign":"%s"}}
3f400ce8 19 = %02x%02x%02x%02x%02x%02x
3f400d04 09 = badge-fl
3f400d10 13 = root:%s:18325:0:99
3f400d2c 1f = flag{from_TechMaker_with_Love}
3f400d74 0c = /etc/shadow
3f400d88 08 = nnc-did
3f400d90 19 = Not available during CTF
...
```

Also I wrote the second version of script which have additional "manual" step - you can point exact addresses to decode if wish:

```bash
./nnc_seg1_decode_manual.py addrs2decode.txt firmware_partitions.bin.3.ota_0.seg1 firmware_partitions.bin.3.ota_0.decoded
=============================================================
====================  AUTO   ================================
=============================================================
3f400020 05 = 7X#+
3f40081e 05 = fm@
3f400861 05 = 5<`
3f400c80 68 = {"flags":["%s","%s","%s","%s","%s","%s","%s","%s"],"timeattack":{"solved":false,"rand":%d,"sign":"%s"}}
3f400ce8 19 = %02x%02x%02x%02x%02x%02x
3f400d04 09 = badge-fl
3f400d10 13 = root:%s:18325:0:99
3f400d2c 1f = flag{from_TechMaker_with_Love}
3f400d74 0c = /etc/shadow
3f400d88 08 = nnc-did
3f400d90 19 = Not available during CTF
3f400dac 08 = <empty>
3f4018e4 1a = https://ctf.techmaker.ua/
3f401900 2b = https://ctf.techmaker.ua/api/flags/2020/%s
3f40192c 3f = flag{%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x}
3f40196c 10 = John the Rooter
3f40197c 54 = Congrats! Don't use fast hashes for passwords and turn that password enter hint off
3f4019d0 11 = Fiasco TransPort
3f4019e4 08 = Z3 1337
3f4019ec 08 = Meet Me
3f4019f4 4b = Congrats! You've managed to bypass cert pinning. Yep, that was an easy one
3f401a40 0f = Take your Time
3f401a50 19 = Obfuscated Mr BeanWalker
3f401a6c 0b = Unsolvable
3f401a78 22 = Congrats! 13117 reverse engineer.
3f401a9c 1b = https://ctf.techmaker.ua/f
3f401ad4 1b = https://ctf.techmaker.ua/f
3f401af8 11 = You found a hidd
3f401b2c 17 = Congrats! IoT devices 
3f401b6c 62 = , including provisioning, patch management, configuration or just simply backdoored by the vendor
3f401bd0 11 = Absolutely ClueL
3f401be4 51 = Congrats! No matter what you choose to use for MQTT auth (PKI or user/pass), rem
3f401c60 65 = Congrats! Now you know how some keygens created. Theorem solvers useful for bypassing challenge-resp
3f401cd8 11 = Congrats! Imagin
3f401d18 37 = ithms without any clue about the principle of locality
3f401d50 33 = Congrats! Don't rely on hardcoded keys and custom 
...
=============================================================
====================  FROM FILE==============================
=============================================================
3f400c80 67 = {"flags":["%s","%s","%s","%s","%s","%s","%s","%s"],"timeattack":{"solved":false,"rand":%d,"sign":"%s"}}
3f400ce8 18 = %02x%02x%02x%02x%02x%02x
3f400d04 0b = badge-flags
3f400d10 1a = root:%s:18325:0:99999:7:::
3f400d2c 1e = flag{from_TechMaker_with_Love}
3f400d74 0b = /etc/shadow
3f400d88 0b = nnc-did
                     *>Á
3f400d90 19 = Not available during CTF�
3f400dac 08 = <empty>

3f4018e4 19 = https://ctf.techmaker.ua/
3f401900 2a = https://ctf.techmaker.ua/api/flags/2020/%s
3f40192c 3e = flag{%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x}
3f40196c 0f = John the Rooter
3f40197c 53 = Congrats! Don't use fast hashes for passwords and turn that password enter hint off
...
```

Result is the same:

(c) **from_TechMaker_with_Love**


## Method 3. Recover key by repeated strings pattern analysis

If you don't have JTAG and stuck at firmware code disasembling you can go other way %)

After getting next flag you got conratulations like (already 6 of them got):
```
Congrats! Don't use fast hashes for passwords and turn that password enter hint off...
Congrats! You've managed to bypass cert pinning. Yep, that was an easy one...
Congrats! 13117 reverse engineer...
Congrats! IoT devices ...
Congrats! No matter what you choose to use for MQTT auth (PKI or user/pass), rem...
Congrats! Now you know how some keygens created. Theorem solvers useful for bypassing challenge-resp...
``` 

But you will not find this strings in plain in binary - they are somehow coded.

Little bit googling on how obfuscate strings in plain C you very probably get this article:

https://blag.nullteilerfrei.de/2019/06/17/diy-string-obfuscation-for-plain-c/

where some Anvol gives his receip (with code):

![AnVol](img/anvol.png)

Due to Anvol also author of NNC 2020 Badge software %) exists big chance that the same method used here too i.e. string obfuscated by xoring orinal string with some code phrase/key.

What we will do next:

1. Find all possible encoded strings with length > len("Congrats!"). 
2. We needn't check all binary due to usually C compiler puts all static data together and checking OTA binary we will not find any strings after 0x20000 offset.
3. Group found candidates by first 8 bytes and discard groups where less 6 members (due to we have at least 6 strings with the same beginning).
4. Due to all these strings supposed start with "Congrats!" and use the same code phrase encoded strings also will start with same (but sure not "Congrats!") bytes.
5. Try get beginning of code phrase xoring first 8 bytes of each group with "Congrats!"
6. Try locate decoded bytes in the binary - if found try decode strings in a group using found key

[Script](scripts/method3/nnc_seg1_decode.py) :

```bash
./nnc_seg1_decode.py part.3.ota_0 results.txt
=============================================================
====================  AUTO   ================================
=============================================================
POSSIBLE KEY FOUND:
00000db4 : b'\x05\x0c\xee\x80\xbe\xbd\xb2\x84a'
b"Congrats! Don't use fast hashes for passwords and turn that pass"
b"Congrats! You've managed to bypass cert pinning. Yep, that was a"
b'Congrats! 13117 reverse engineer.'
b'Congrats! IoT devices '
b'Congrats! No matter what you choose to use for MQTT auth (PKI or'
b'Congrats! Now you know how some keygens created. Theorem solvers'
b'Congrats! Imagin'
b"Congrats! Don't rely on hardcoded keys and custom "
```
and check _results.txt_ for decoded strings and

(c) **from_TechMaker_with_Love**
