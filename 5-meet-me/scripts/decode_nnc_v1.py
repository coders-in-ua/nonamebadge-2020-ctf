#!/usr/bin/python
import binascii
import hashlib
import sys
from ecdsa import numbertheory, util
from ecdsa import VerifyingKey, SigningKey
from os.path import commonprefix
from binascii import hexlify, unhexlify




PUBLIC_KEY = """
-----BEGIN PUBLIC KEY-----
MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAE5UQ/BT52Hda1iGUfrgifjNykXpLD
YRY3hSNrqYODGQOfWHbOP8NeN2KMlRazbg92viuDiKnx8Z/ngvWMqi85Og==
-----END PUBLIC KEY-----
"""

PRIVATE_DER_FILE = "private.der"

vk = VerifyingKey.from_pem(PUBLIC_KEY.strip())

# Firmware Digest: 
msg1_sha256 = '53f44252897b4a16ba11d290d0a807daf65023d4b13af9885b8d625fde88d071'

# Firmware Signature: 
sig1 = '2faab997dc7495a56a141fc767498ea0b063ae4a91bc4055453fed67e79f055f714da47824f560d171918e6924dc77a64053e2d24baeb6544d49580bca402751'


# Partition Table Digest: 
msg2_sha256 = 'ecf4641b458abda784e2f1e44907cd6a1058d4663299c0f707f9853f1fabf662'

# Partition Table Signature: 
sig2 = '2faab997dc7495a56a141fc767498ea0b063ae4a91bc4055453fed67e79f055fce44222941b007a5cb9ff0dc03bb046fa7c7c62c4e9242b34946b1e5c4df5f85'


res = commonprefix( (sig1, sig2) )
prefix = len(res)
print('common prefix len = %s' % prefix)

r  = int(sig1[:prefix], 16)
s1 = int(sig1[prefix:], 16)
s2 = int(sig2[prefix:], 16)
z1 = int(msg1_sha256, 16)
z2 = int(msg2_sha256, 16)

k = (z1 - z2) * numbertheory.inverse_mod(s1 - s2, vk.pubkey.order) % vk.pubkey.order
d = (s1 * k - z1) * numbertheory.inverse_mod(r, vk.pubkey.order) % vk.pubkey.order

sk = SigningKey.from_secret_exponent(d, curve=vk.curve, hashfunc=hashlib.sha256)

pem = sk.to_pem()
print("PEM:\n{}\n".format(pem.decode()))

der = sk.to_der()

with open(PRIVATE_DER_FILE, "wb+") as file:
  file.write(der)
print("DER key writed to: {}\n".format(PRIVATE_DER_FILE))

der_hex = hexlify(der)
print("DER HEX:\n{}\n".format(der_hex))
print("SHA256(DER):\n{}\n".format(hashlib.sha256(der).hexdigest()))
print("SHA256(DER_HEX):\n{}\n".format(hashlib.sha256(der_hex).hexdigest()))
