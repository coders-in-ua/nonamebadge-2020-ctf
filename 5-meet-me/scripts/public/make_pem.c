#include <stdio.h>
#include <openssl/ec.h>
#include <openssl/pem.h>

const char *xHex = "b6512e3b92d7ba703550e2c6ba6e71673e84f77add106b63f03d68c949fcd2b5";
const char *yHex = "3f0961af5a300ec16733697035f0e6f1142819c21d07fed851ba1392ab02bbfb";

int main(
    int argc,
    char **argv)
{
    EC_KEY *eckey = NULL;
    BIGNUM *x = NULL, *y = NULL;

    eckey = EC_KEY_new_by_curve_name(NID_X9_62_prime256v1);

    BN_hex2bn(&x, xHex);
    BN_hex2bn(&y, yHex);

    EC_KEY_set_public_key_affine_coordinates(eckey, x, y);
    EC_KEY_set_asn1_flag(eckey, OPENSSL_EC_NAMED_CURVE);
    PEM_write_EC_PUBKEY(stdout, eckey);

    BN_free(x);
    BN_free(y);
    EC_KEY_free(eckey);
}