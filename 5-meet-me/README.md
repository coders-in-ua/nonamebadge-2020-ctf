# 5. Meet Me

Analyzing pcap file in the task 3 ( ["Absolutely ClueLess"](3-absolutely-clueless/README.md) ) I saw encrypted traffic to few servers.

Let's MITM it.

## Step 1. App signing

First of all I checked if application signed - it could be done analyzing binary or just disassemble/assemble using tools from previous NNC2019 CTF: 
[Badge Firmware builder](https://gitlab.com/coders-in-ua/nonamebadge-2019-ctf/-/blob/master/Extra/1.%20Firmware%20builder/README.md) 

Sure it is %)

We need to somehow bypass signing.

There are few possible ways

### Method 1. Restore private key

We can restore private key as described in [Ployka PWNer](https://gitlab.com/coders-in-ua/nonamebadge-2019-ctf) write-up.

Only one notice - to run scripts we need public key or change scripts. 

Initially we hadn't public key but later in CTF chat public key was given.

#### Without public key

If check ESP_IDF documentation we can read that only one curve is allowed/used for application signing - NIST256p (synonyms: prime256v1,secp256r1 ). Just update script to use it and restore private key:

```python
#!/usr/bin/python
import binascii
import hashlib
import sys
from ecdsa import numbertheory, util, curves
from ecdsa import VerifyingKey, SigningKey
from os.path import commonprefix
from binascii import hexlify, unhexlify


PRIVATE_DER_FILE = "private.der"

# esp-idf-4.1-stable/components/bootloader/subproject/components/micro-ecc/micro-ecc/curve-specific.inc
vk_curve = curves.NIST256p # synonyms: prime256v1, secp256r1
vk_pubkey_order = vk_curve.order

# Firmware Digest: 
msg1_sha256 = '53f44252897b4a16ba11d290d0a807daf65023d4b13af9885b8d625fde88d071'

# Firmware Signature: 
sig1 = '2faab997dc7495a56a141fc767498ea0b063ae4a91bc4055453fed67e79f055f714da47824f560d171918e6924dc77a64053e2d24baeb6544d49580bca402751'


# Partition Table Digest: 
msg2_sha256 = 'ecf4641b458abda784e2f1e44907cd6a1058d4663299c0f707f9853f1fabf662'

# Partition Table Signature: 
sig2 = '2faab997dc7495a56a141fc767498ea0b063ae4a91bc4055453fed67e79f055fce44222941b007a5cb9ff0dc03bb046fa7c7c62c4e9242b34946b1e5c4df5f85'


res = commonprefix( (sig1, sig2) )
prefix = len(res)
print('common prefix len = %s' % prefix)

r  = int(sig1[:prefix], 16)
s1 = int(sig1[prefix:], 16)
s2 = int(sig2[prefix:], 16)
z1 = int(msg1_sha256, 16)
z2 = int(msg2_sha256, 16)

k = (z1 - z2) * numbertheory.inverse_mod(s1 - s2, vk_pubkey_order) % vk_pubkey_order
d = (s1 * k - z1) * numbertheory.inverse_mod(r, vk_pubkey_order) % vk_pubkey_order

sk = SigningKey.from_secret_exponent(d, curve=vk_curve, hashfunc=hashlib.sha256)

pem = sk.to_pem()
print("PEM:\n{}\n".format(pem.decode()))

der = sk.to_der()

with open(PRIVATE_DER_FILE, "wb+") as file:
  file.write(der)
print("DER key writed to: {}\n".format(PRIVATE_DER_FILE))

der_hex = hexlify(der)
print("DER HEX:\n{}\n".format(der_hex))
print("SHA256(DER):\n{}\n".format(hashlib.sha256(der).hexdigest()))
print("SHA256(DER_HEX):\n{}\n".format(hashlib.sha256(der_hex).hexdigest()))
```

#### Restore public key

Exists two way - easy and hard %)

Hard way - reverse code and find how and where public key stored:
1. Stored only X and Y components (32 bytes each).
2. Curve not stored due to it is hard-coded.
3. To find where it is stored you can:

**Hard way:** analyze source codes of bootloader (it is opensource), find function of verification of application:
```
esp_secure_boot_verify_signature()
esp_secure_boot_verify_ecdsa_signature_block()
```
than find were defined public key are stored:
```bash
**secure_boot_signatures.c:**
extern const uint8_t signature_verification_key_start[] asm("_binary_signature_verification_key_bin_start");
extern const uint8_t signature_verification_key_end[] asm("_binary_signature_verification_key_bin_end");
```
(check ESP-ID folder **components/bootloader_support/src/idf/**)

Next reverse binary, find verification function and offset of key storage %))


**Easy way:** Just build your own bootloader with own application signing key and same (or about the same as original bootloader) build options and check were signing key is in your bootloader binary. After try locate signing key in the same or close offset in the original binary. 

After getting 64 bytes of signing key just convert it to the convinient format (pem or der). 
```C
#include <stdio.h>
#include <openssl/ec.h>
#include <openssl/pem.h>

const char *xHex = "b6512e3b92d7ba703550e2c6ba6e71673e84f77add106b63f03d68c949fcd2b5";
const char *yHex = "3f0961af5a300ec16733697035f0e6f1142819c21d07fed851ba1392ab02bbfb";

int main(
    int argc,
    char **argv)
{
    EC_KEY *eckey = NULL;
    BIGNUM *x = NULL, *y = NULL;

    eckey = EC_KEY_new_by_curve_name(NID_X9_62_prime256v1);

    BN_hex2bn(&x, xHex);
    BN_hex2bn(&y, yHex);

    EC_KEY_set_public_key_affine_coordinates(eckey, x, y);
    EC_KEY_set_asn1_flag(eckey, OPENSSL_EC_NAMED_CURVE);
    PEM_write_EC_PUBKEY(stdout, eckey);

    BN_free(x);
    BN_free(y);
    EC_KEY_free(eckey);
}
```

Having public key we can use original NNC2019 writup scripts with no change (sure we have to update inoput data first %)

Having private key we can change firmware as we wish: [Firmware Builder](https://gitlab.com/coders-in-ua/nonamebadge-2019-ctf/-/tree/master/Extra/1.%20Firmware%20builder).

### Method 2. Custom booloader

Just build your own bootloader which not verifles application. It is possible due bootloader is not signed in the Badge 2020 and we can reflash it. To build booloader I used [NoNameBadge 2019 - Public Version](https://gitlab.com/techmaker/nnc-badge-2019) only changed partitions table to 0x9000 (due to new badge has it at 0x9000 not default 0x8000).

After build the bootloader is at **build/bootloader/bootloader,bin** and to flash it just run:

[Custom Booloader](scripts/bootloader_custom/)
```bash 
esptool.py --chip esp32 --port /dev/ttyUSB0 --baud 2000000 --before default_reset --after hard_reset write_flash -z --flash_mode dio --flash_freq 40m --flash_size detect 0x1000 bootloader.bin
```


### Meyhod 3. Replace public key and use own signing key

Similar to the Method 1 but after finding public key in the bootloader binary we change it to custom public key (sure we have to generate public/private key pair first as it described in the ESP-IDF SDK).

After that we can sign application with our private key.



## Step 2. Server's CA check

Absolutly the same as in the [NNC 2019 CTF MITM](https://gitlab.com/coders-in-ua/nonamebadge-2019-ctf/-/blob/master/Extra/3.%20MITM/part2/README.md)


### Method 1. Replace CA cert

When we can patch binary the can replace public CA certificate in it.

But surprize - if you will Burp for MITM it generates RSA certificates which much bigger of the DSA certificate used in the badge firmware. 
Variants:
1) create self-signed DSA certificates pair and use it in Burp;
2) add (inject %) our certificate at the end of segment (seg1) and change references in binary to the new certificate.

I chose the first way - try create shorter CA certificate.

How to create and import custom CA to Burp is descrebed well [here](https://medium.com/hengky-sanjaya-blog/creating-ssl-certificate-burp-suite-960e375502aa).

**Let's go**

Check Techmaker's cert:

```bash
> openssl x509 -text -noout -in ca_techmaker.pem 

Certificate:
    Data:
        Version: 3 (0x2)
        Serial Number:
            44:af:b0:80:d6:a3:27:ba:89:30:39:86:2e:f8:40:6b
        Signature Algorithm: sha1WithRSAEncryption
        Issuer: O = Digital Signature Trust Co., CN = DST Root CA X3
        Validity
            Not Before: Sep 30 21:12:19 2000 GMT
            Not After : Sep 30 14:01:15 2021 GMT
        Subject: O = Digital Signature Trust Co., CN = DST Root CA X3
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
                RSA Public-Key: (2048 bit)
                Modulus:
                    00:df:af:e9:97:50:08:83:57:b4:cc:62:65:f6:90:
                    ...
                    02:5d
                Exponent: 65537 (0x10001)
        X509v3 extensions:
            X509v3 Basic Constraints: critical
                CA:TRUE
            X509v3 Key Usage: critical
                Certificate Sign, CRL Sign
            X509v3 Subject Key Identifier: 
                C4:A7:B1:A4:7B:2C:71:FA:DB:E1:4B:90:75:FF:C4:15:60:85:89:10
    Signature Algorithm: sha1WithRSAEncryption
         a3:1a:2c:9b:17:00:5c:a9:1e:ee:28:66:37:3a:bf:83:c7:3f:
         ...
         82:35:35:10
```
It's size is 1182 bytes. We should to fit in this limits. 

Let's check Burp's generated certificate:

```bash
> openssl x509 -text -noout -in ca_burp_public.der -inform der

Certificate:
    Data:
        Version: 3 (0x2)
        Serial Number: 1418836914 (0x5491bbb2)
        Signature Algorithm: sha256WithRSAEncryption
        Issuer: C = PortSwigger, ST = PortSwigger, L = PortSwigger, O = PortSwigger, OU = PortSwigger CA, CN = PortSwigger CA
        Validity
            Not Before: Dec 17 17:21:54 2014 GMT
            Not After : Feb 25 17:21:54 2023 GMT
        Subject: C = PortSwigger, ST = PortSwigger, L = PortSwigger, O = PortSwigger, OU = PortSwigger CA, CN = PortSwigger CA
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
                RSA Public-Key: (2048 bit)
                Modulus:
                    00:e0:dd:57:4a:74:06:eb:41:dc:37:fa:2f:c0:1d:
                    ...
                    a9:69
                Exponent: 65537 (0x10001)
        X509v3 extensions:
            X509v3 Basic Constraints: critical
                CA:TRUE, pathlen:0
            X509v3 Subject Key Identifier: 
                B0:9D:AB:FC:F9:70:E0:4F:40:3D:AA:A7:5F:44:92:62:9E:6B:68:E0
    Signature Algorithm: sha256WithRSAEncryption
         83:33:ac:c8:03:0d:56:60:42:73:10:21:6b:83:30:f9:52:a4:
         ...
         23:30:9f:31

```
Size is 1375 (could be little bit different but always size is near that value).
Difference in size is due to:
A) Techmaker's certificate uses SHA1 signature (20 bytes), Burp's - SHA256 (32 bytes)
B) Techmaker's certificate has shorter subject/issue size.

That I did (very important to fill as shorter as possible data - I filled only OU cnd CN as "PT"):

```bash
> openssl req -newkey rsa:2048 -nodes -keyout custom_private_sha1.pem -x509 -sha1 -days 365 -out custom_public_sha1.pem

Generating a RSA private key
.......................................................+++++
...............................................................................................................................................+++++
writing new private key to 'custom_private_sha1.pem'
-----
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [AU]:
State or Province Name (full name) [Some-State]:.
Locality Name (eg, city) []:.
Organization Name (eg, company) [Internet Widgits Pty Ltd]:.
Organizational Unit Name (eg, section) []:PT
Common Name (e.g. server FQDN or YOUR name) []:PT
Email Address []:
```
If you got _custom_public_sha1.pem_ larger than 1182 bytes try one time more ^)

Check (issue should contain only 3 fields):

```bash
> openssl x509 -text -noout -in custom_public_sha1.pem

Certificate:
    Data:
        Version: 3 (0x2)
        Serial Number:
            60:10:8f:46:fa:5f:48:0e:b3:d7:1f:11:75:24:a5:d9:71:85:10:8f
        Signature Algorithm: sha1WithRSAEncryption
        Issuer: C = AU, OU = PT, CN = PT
        Validity
            Not Before: Dec 18 06:06:52 2020 GMT
            Not After : Dec 18 06:06:52 2021 GMT
        Subject: C = AU, OU = PT, CN = PT
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
                RSA Public-Key: (2048 bit)
```

Combine both public and private in one file:

```bash
openssl pkcs12 -inkey custom_private_sha1.pem -in custom_public_sha1.pem -export -out custom_full_sha1.p12
```

Will pad public key by zeroe's to 1182 bytes (in my case plus 18 bytes), I pad due to I put new certificate exactly instead old one.
Minimal pad size iz one zero byte (end of string in C language):

```bash
cp custom_public_sha1.pem custom_public_sha1_padded.pem
dd if=/dev/zero bs=1 count=18 >> custom_public_sha1_padded.pem
```

and import _custom_full_sha1.p12_ to Burp and replace certificate in firmware (seg1) with _custom_public_sha1_padded.pem_ contents:

After setup transparent proxy on 443 port in Burp (443 port is https port and transparent due to our badge nothing knows about proxy %) and flasshing patched software (don't forget assemble it after seg1 update!):

![Proxy](img/burp_00.png)

![Proxy](img/burp_01.png)

run badge (reset, run _flags_ command) and

![Proxy](img/burp_02.png)

yep, it works and we see "secret" traffic:

![Proxy](img/burp_03.png)

Very promising looks next intercepted PUT request:

```
PUT /api/flags/2020/1105d536b308990c76c15b597edf723a HTTP/1.1
User-Agent: BadgeOS HTTP Client
Host: ctf.techmaker.ua
Content-Type: application/json
Content-Length: 251
Connection: close

{"flags":["flag{5209d0c6b0f0a57891a486375296}","flag{3fa83be12fc51644aa0387e53924}","flag{5493ae98b23b28d5d41b599d8321}","flag{z39f34578ac}","<empty>","<empty>","<empty>","<empty>"],"timeattack":{"solved":false,"rand":340679415,"sign":"ca92e4927c4e"}}

```

### Method 2. Four-zeros method

Solution is the same as descibed in [4 bytes solution](https://gitlab.com/coders-in-ua/nonamebadge-2019-ctf/-/blob/master/Extra/3.%20MITM/part2/README_0x4.md) i.e. open binary in Radare2/IDA/Ghidra, find CA public key in it and X-ref to it. Than just replace X-Ref by zeroes.

I.e.:

1. Downlaod OTA partition (partitioan address and size "leaked" in output of badge version command:

![Version](img/version.png)

[read_ota0.sh](scripts/fourbytes/read_ota0.sh)
```bash
#!/bin/bash

# 1920k = 0x1e0000
esptool.py -p /dev/ttyUSB0 -b 2000000 read_flash 0x20000 0x1e0000 badge_ota0.bin
```

In Extra section of these writups I will show how to get all partitions info directly from binary %)

2. Disassemble it:

```bash
./nnc_disassemble.py badge_ota0.bin

Image version: 1
Entry point: 400810f8
secure_pad: False
flash_mode: 2
flash_size_freq: 47
7 segments

Segment 1: len 0x1a454 load 0x3f400020 file_offs 0x00000018
  addr=0x3f400020 file_offs=0x18 include_in_checksum=True

Segment 2: len 0x03c54 load 0x3ffbdb60 file_offs 0x0001a474
  addr=0x3ffbdb60 file_offs=0x1a474 include_in_checksum=True

Segment 3: len 0x00404 load 0x40080000 file_offs 0x0001e0d0
  addr=0x40080000 file_offs=0x1e0d0 include_in_checksum=True

Segment 4: len 0x01b34 load 0x40080404 file_offs 0x0001e4dc
  addr=0x40080404 file_offs=0x1e4dc include_in_checksum=True

Segment 5: len 0xb96d8 load 0x400d0020 file_offs 0x00020018
  addr=0x400d0020 file_offs=0x20018 include_in_checksum=True

Segment 6: len 0x1c3e4 load 0x40081f38 file_offs 0x000d96f8
  addr=0x40081f38 file_offs=0xd96f8 include_in_checksum=True

Segment 7: len 0x00064 load 0x400c0000 file_offs 0x000f5ae4
  addr=0x400c0000 file_offs=0xf5ae4 include_in_checksum=True

Checksum: 5b (valid)
Validation Hash: 2ba8bd442cd96e414a018f83a29f9cbccb9b60366701d3600efb522b3b487edd (valid)
END
```

Open downloaded binary in Cutter/Radare2/IDA/Ghidra.

But I recomend open ELF file (later I will show how make ELF from binary).

You can find ELF **part.3.ota_0.elf** in [parsed binary](extra/firmware/0-CustomBootloader/parset-syms/) folder

Locate beginning of CA certificate in binary:

![Cert](img/four_00.png)

Find XRef to it:

![Cert](img/four_01.png)

![Cert](img/four_02.png)

![Cert](img/four_03.png)

Address of CA is 0x3f402943 and stored in 0x400d06e8 .

We need just rewrite it by zeroes.

Address 0x400d06e8 corresponds to segment 5:

```
Segment 5: len 0xb96d8 load 0x400d0020 file_offs 0x00020018
  addr=0x400d0020 file_offs=0x20018 include_in_checksum=True
```

Offset in segment file is 0x400d06e8-0x400d0020=0x6c8 (or just locate searching bytes sequence 43 29 40 3f, i.e. 0x3f402943 in Little Endian):


![Cert](img/four_04.png)

![Cert](img/four_05.png)

![Cert](img/four_06.png)

Assemble back:

```bash
/nnc_assemble.py badge_ota0.bin.map nnc_badge_patched.bin
/home/vvs/.espressif/python_env/idf4.1_py2.7_env/local/lib/python2.7/site-packages/cryptography/__init__.py:39: CryptographyDeprecationWarning: Python 2 is no longer supported by the Python core team. Support for it is now deprecated in cryptography, and will be removed in a future release.
  CryptographyDeprecationWarning,
Entrypoint: 400810f8
The number of segments: 7
1: file=badge_ota0.bin.seg1 map=3f400020 file_ofs=00000018 in_checksum=True
2: file=badge_ota0.bin.seg2 map=3ffbdb60 file_ofs=0001a474 in_checksum=True
3: file=badge_ota0.bin.seg3 map=40080000 file_ofs=0001e0d0 in_checksum=True
4: file=badge_ota0.bin.seg4 map=40080404 file_ofs=0001e4dc in_checksum=True
5: file=badge_ota0.bin.seg5 map=400d0020 file_ofs=00020018 in_checksum=True
6: file=badge_ota0.bin.seg6 map=40081f38 file_ofs=000d96f8 in_checksum=True
7: file=badge_ota0.bin.seg7 map=400c0000 file_ofs=000f5ae4 in_checksum=True
```

Write:
(write_ota0_patched.sh)[scripts/fourbytes/write_ota0_patched.sh]

```bash
esptool.py -p /dev/ttyUSB0 -b 2000000 write_flash 0x20000 nnc_badge_patched.bin
```

login to badge and...

![Cert](img/four_success.png)

you got a flag!!!


## Step 3. MITM

Now we ready to MITM

### Method 1. ARP and/or DNS spoofing

Classical hacker's way. There are a huge amount of write-ups how to do this (for example [here](https://hackernoon.com/man-in-the-middle-attack-using-bettercap-framework-hd783wzy) - just one of top googled results %)

Only one thing - don't forget arp-spoof in both directions i.e. route not only traffic from victiom to router but from router to victim too. For Bettercap it us full duplex mode).

### Method 2. WiFi sandbox + static DNS record + proxy

But in my network ARP/DNS spoofing failed - it is good for my network but bad for CTF %)

That why I had to install Wi-Fi sandbox + used static DNS for ctf.techmaker.ua + proxy and intercepted desired traffic.




