# 3. Absolutely ClueLess

First let intercept traffic from badge.

Due to my badge was connected to Mikrotik Wi-Fi router which has built-in sniffer - I just saved some traffic to/from badge to pcap file and opened in Wireshark

**Notice**: during intercepting traffic I pushed all buttons on badge, reset it, executed all available commands in the badge console.

Other way - to use arp spoofing (for example, bettercap) to redirect traffic to/from badge to your computer.

## Wireshark

Opening pcap file in Wireshark we see that our badge communicates to some server, most connection are TLS emcrypted but one is not - on port 2883:

![Wireshark](img/wireshark_01.png)

Try "Follow" in context menu for this stream:

![Wireshark](img/wireshark_03.png)

![Wireshark](img/wireshark_02.png)

We see that some "telemetry" sent to server but protocol is not decoded.

Little bit googling and thinking and I realized that it is MQTT protocol but on non-standard port.

Let's change MQTT decoder port in Wireshark's preferences:

![Wireshark](img/wireshark_04.png)

and check stream:

![Wireshark](img/wireshark_05.png)

![Wireshark](img/wireshark_06.png)

Much better %) 

And... we see auths data sent in plain form!!!


## MQTT server

Using found credential let's login to remote server.

I use MQTTExplorer for this:

![MQTT](img/mqtt_00.png)

After connection we will find that can read ALL data on server:

![MQTT](img/mqtt_01.png)

Some of this data are kindly requests to get flag %)

Let's ask for flag too (kindly!!!).

I did the similar request putting DeviceID into the request (DeviceID shown in **version** output in badge console):

![MQTT](img/version.png)

![MQTT](img/mqtt_02.png)

As only flag published (we will see it on the left pane) we will see corresponding notification in CTF chat.

But we need manually register it on the badge:

![flag](img/flag.png)


