# 8. Unsolvable

## Intro

After doing _"Obfuscated Mr BeanWalker"_ task I found interesting function which uses string:

```
flag{%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x}
```

Looks like generation function or very close to it.

Digging further I found that it is used as format string in sprintf function call:

![print flag 1](img/print_flag_00.png)

![print flag 1](img/print_flag_01.png)

f0..f13 in comments are bytes that printed used this mask

## Method 1. JTAG

Just set breakpoint at 400d9f80 and see resulted string:

![print flag 1](img/print_flag_02.png)

![print flag 1](img/jtag_00.png)

Repeating few times we will get all flags including "Unsolvable".

## Method 2. Code patching

Now we will try force our badge generate and print all flags to us.

Flag generation function uses sprintf which prints into the buffer:

```
int sprintf ( char * str, const char * format, ... );
```

But C language also has similar functions which allow print to somewhere else:

```
int fprintf ( FILE * stream, const char * format, ... );

int printf ( const char * format, ... );
```

The first idea was replace 
```
sprintf(buf, format, ...)
```

with 

```
fprintf(stdout, format, ...)
```

due to they have the same amount of arguments but I failed this way.

After I tried (successfully) replace sprintf with printf.

But what about the extra "buf" argument used by sprintf and needn't by printf?

If left as it is printf will print "buf" and first 13 bytes of flag instead of printing 14 bytes of flag (or just exception arises due to buffer offset will not fit in %02x %). And the last byte of flag will be "forgotten".

Here is I did one trick - I replaced value in **a11** register (stored address of first argument i.e. buffer) with last byte of flag:

![print flag 1](img/patch_00.png)

6 bytes which made the world:

![print flag 1](img/patch_02.png)

Assembling, flashing and... not bricked (Have you forgot to backup firmware?). Good %) :

![print flag 1](img/patch_04.png)

We see one flag but it is not accepted (even if we not forgot about shifted bytes in printed flag).

It is because we broke the flag verification logic and instead of generation of flag (returning it to caller) it prints it (returning unchanged buffer to caller).

Also we don't see the rest of flags. But if one successfully printed the rest must be printed too!!!

I switched on logging in Putty to catch all output in terminal and.... voila:

![print flag 1](img/patch_03.png)

Don't forget that bytes of flag printed in order 13, 0, 1, ... 12 :

![print flag 1](img/flags.png)

To register them we need revert to non-patched firmware (remeber about backups? %) :

![print flag 1](img/patch_05.png)

![print flag 1](img/patch_06.png)


![print flag 1](img/unnamed.jpg)

[The END](https://youtu.be/-tJYN-eG1zk?list=PLQy8uETE-gDEe1ptRV87EA73dGS2xTgAN)
